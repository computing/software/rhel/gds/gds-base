%define name gds-base
%define version 3.0.1
%define release 1.1
%define daswg   /usr
%define prefix  %{daswg}
%define _sysconfdir /etc
%define config_runflag --sysconfdir=%{_sysconfdir}
%if %{?_verbose}%{!?_verbose:0}
# Verbose make output
%define _verbose_make 1
%else
%if 1%{?_verbose}
# Silent make output
%define _verbose_make 0
%else
# DEFAULT make output
%define _verbose_make 1
%endif
%endif

Name: 		%{name}
Summary: 	GDS package Core libraries
Version: 	%{version}
Release: 	%{release}%{?dist}
License: 	GPL
Group: 		LSC Software/Data Analysis
Source: 	https://software.igwn.org/lscsoft/source//%{name}-%{version}.tar.gz
Patch0:     gdscntr.pc.in.diff
Packager: 	John Zweizig (john.zweizig@ligo.org)
BuildRoot: 	%{_tmppath}/%{name}-%{version}-root
URL: 		http://www.lsc-group.phys.uwm.edu/daswg/projects/dmt.html
Prefix:		      %prefix
BuildRequires: 	gcc, gcc-c++, glibc, automake, autoconf, libtool, m4, make
BuildRequires:  gzip zlib bzip2 expat-devel ldas-tools-framecpp
BuildRequires:  ldas-tools-framecpp-devel ldas-tools-al-devel
BuildRequires:  libmetaio-devel
BuildRequires:  libcurl-devel zlib-devel krb5-devel
BuildRequires:  readline-devel fftw-devel cyrus-sasl-devel
BuildRequires:  jsoncpp-devel
BuildRequires:  boost-devel
Requires:       expat
Requires:       fftw
Requires:       cyrus-sasl
Requires:       libmetaio
Requires:       jsoncpp
Obsoletes:      gds-core <= 2.18.19
Obsoletes:      %{name}-all <= 2.18.19
Obsoletes:      gds-utilities-base <= 3.0.1

%description
Core libraries required by the rest of the GDS packages

%package -n gds-framexmit
Summary: 	GDS package low-latency libraries
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name} = %{version}-%{release}
Obsoletes:      gds-core < 2.18
Obsoletes:      gds-services < 2.18

%description -n gds-framexmit
Lowlatency libraries implement the gds shared memory and interconnects.

%package -n gds-framexmit-headers
Summary: 	GDS header files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name}-headers = %{version}-%{release}

%description -n gds-framexmit-headers
 GDS software header files.

%package -n gds-framexmit-devel
Summary: 	GDS development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       gds-framexmit = %{version}-%{release}
Requires:       gds-framexmit-headers = %{version}-%{release}
Requires:       %{name}-devel = %{version}-%{release}

%description -n gds-framexmit-devel
 GDS software development files.

%package crtools
Summary: 	Core shared objects
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       %{name} = %{version}-%{release}
Requires:       %{name}-headers = %{version}-%{release}
Requires:       libcurl

%description crtools
GDS control room tools

%package devel
Summary: 	GDS development files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       gds-framexmit-devel = %{version}-%{release}
Requires:       gds-services = %{version}-%{release}
Requires:       %{name}-headers = %{version}-%{release}
Requires:       expat-devel, cyrus-sasl-devel, ldas-tools-framecpp-devel

%description devel
GDS software development files.

%package headers
Summary: 	GDS header files.
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires:       gds-framexmit-headers = %{version}-%{release}
Requires:       boost-devel
Obsoletes:      %{name}-devel < 2.18

%description headers
GDS software header files.

%package root
Summary: 	 Root wrappers for gds libraries
Version: 	 %{version}
Group: 		 LSC Software/Data Analysis
Requires:  %{name} = %{version}-%{release}
Obsoletes: gds-core-root < 2.18

%description root
Root wrappers for gds class libraries and macros/classes for inteactive use
of gds libraries via root.

%package  monitors
Summary: 	Libraries used by DMT Monitor programs
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Requires: %{name} = %{version}-%{release}

%description monitors
Libraries used by DMT Monitor programs

%package   -n gds-services
Summary: 	 GDS services
Version: 	 %{version}
Group: 		 LSC Software/Data Analysis
Requires:  %{name} = %{version}-%{release}
Requires:  %{name}-monitors = %{version}-%{release}
Obsoletes: gds-core < 2.18

%description -n gds-services
GDS runtime services

%package web
Summary: 	DMT web services
Version: 	%{version}
Group: 		LSC Software/Data Analysis
Provides: gds-utilities-web
Requires: %{name} = %{version}-%{release}, libcurl

%description web
DMT web services

%prep
%setup -q
%patch0 -p1

%build
PKG_CONFIG_PATH=%{daswg}/%{_lib}/pkgconfig
ROOTSYS=/usr

%if 0%{?rhel} < 8
CXXFLAGS="-std=c++11"
export CXXFLAGS
%endif

export PKG_CONFIG_PATH ROOTSYS

./configure  \
    --prefix=%prefix \
    --libdir=%{_libdir} \
	  --includedir=%{prefix}/include/gds \
	  --enable-online \
    --enable-dtt \
    %{config_runflag}
make V=%{_verbose_make}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# don't distribute *.la files
[ ${RPM_BUILD_ROOT} != "/" ] && find ${RPM_BUILD_ROOT} -name "*.la" -type f -delete

%files
%defattr(-,root,root)
%{_libdir}/libdaqs.so*
%{_libdir}/libdmtsigp.so*
%{_libdir}/libframefast.so*
%{_libdir}/libframeutil.so*
%{_libdir}/libgdsbase.so*
%{_libdir}/libgdscntr.so*
%{_libdir}/libgdsmath.so*
%{_libdir}/libhtml.so*
%{_libdir}/libjsstack.so*
%{_libdir}/liblmsg.so*
%{_libdir}/liblxr.so*
%{_libdir}/libparsl.so*
%{_libdir}/libsockutil.so*
%{_libdir}/libweb.so*
%{_libdir}/libxsil.so*
%{_bindir}/lmsg_status
%{_bindir}/when
%{_datadir}/gds/html-styles

%files -n gds-framexmit
%{_libdir}/libframexmit.so.*

%files crtools
%defattr(-,root,root)
%{_libdir}/libclient.so*
%{_libdir}/libgdsalgo.so*
%{_libdir}/libdmtaccess.so*
%{_libdir}/libmondmtsrvr.so*
%{_libdir}/libmonlmsg.so*

%files -n gds-framexmit-headers
%defattr(-,root,root)
%{_includedir}/gds/framexmit

%files -n gds-framexmit-devel
%defattr(-,root,root)
%{_libdir}/pkgconfig/gds-framexmit.pc
%{_libdir}/libframexmit.a
%{_libdir}/libframexmit.so

%files devel
%defattr(-,root,root)
%exclude %{_libdir}/pkgconfig/gds-framexmit.pc
%exclude %{_libdir}/libframexmit.a
%{_libdir}/pkgconfig/*
%{_libdir}/*.a

%files headers
%defattr(-,root,root)
%exclude %{_includedir}/gds/framexmit
%{_includedir}/gds/*

%files -n gds-services
%defattr(-,root,root)
%{_bindir}/Alarm
%{_bindir}/AlarmMgr
%{_bindir}/dmt_nameserver
%{_bindir}/NameCtrl
%{_bindir}/no_insert
%{_bindir}/TrigMgr
%{_bindir}/trigRmNode
%{_bindir}/TrigRndm
%{_libdir}/libgdstrig.so*
%{_libdir}/libserver.so*

%files monitors
%defattr(-,root,root)
%{_libdir}/libtclient.so*
%{_libdir}/libmonitor.so*

%files web
%defattr(-,root,root)
%{_bindir}/GraphIt
%{_bindir}/webview
%{_bindir}/webxmledit

%changelog
# date +'%a %b %d %Y'
* Mon Mar 11 2024 Adam Mercer <adam.mercer@ligo.org> - 3.0.1-1.1
- Patch gdscntr.pc for @GDS_VERSION@

* Tue Mar 05 2024 Edwbbard Maros <ed.maros@ligo.org> - 3.0.1-1
- Updated for release as described in NEWS.md

* Sat Nov 19 2022 Edwbbard Maros <ed.maros@ligo.org> - 3.0.0-1
- Updated for release as described in NEWS.md

* Thu Nov 17 2022 Edwbbard Maros <ed.maros@ligo.org> - 2.19.11-1
- Updated for release as described in NEWS.md

* Wed Nov 02 2022 Edward Maros <ed.maros@ligo.org> - 2.19.10-1
- Updated for release as described in NEWS.md

* Thu Aug 12 2021 Edward Maros <ed.maros@ligo.org> - 2.19.8-1
- View NEWS.md for list of changes

* Thu Aug 12 2021 Edward Maros <ed.maros@ligo.org> - 2.19.7-1
- View NEWS.md for list of changes

* Wed May 19 2021 Edward Maros <ed.maros@ligo.org> - 2.19.6-1
- View NEWS.md for list of changes

* Fri May 07 2021 Edward Maros <ed.maros@ligo.org> - 2.19.5-1
- View NEWS.md for list of changes

* Wed Feb 17 2021 Edward Maros <ed.maros@ligo.org> - 2.19.4-1
- Removed python configuration
- Modified Source field of RPM spec file to have a fully qualified URI

* Tue Feb 02 2021 Edward Maros <ed.maros@ligo.org> - 2.19.3-1
- Corrections to build rules

* Fri Nov 20 2020 Edward Maros <ed.maros@ligo.org> - 2.19.2-1
- Split lowlatency as a separate package

* Thu Jun 11 2020 Edward Maros <ed.maros@ligo.org> - 2.19.1-1
- Removed FrameL
